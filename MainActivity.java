package com.example.vvost.urljsontest;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.HandlerThread;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {
    private TextView tvData;
    StringBuffer buffer;
    DatabaseReference databaseReference;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        buffer = new StringBuffer();
        FirebaseApp.initializeApp(this);
        ListView lv = (ListView) findViewById(R.id.listViewCountry);
        ArrayList<String> arrayCountry = new ArrayList<>();
        // arrayCountry.addAll(Arrays.asList(getResources().getStringArray(R.array.array_country)));

        adapter = new ArrayAdapter<>(
                MainActivity.this,
                android.R.layout.simple_list_item_1,
                arrayCountry);
        lv.setAdapter(adapter);
        tvData = (TextView) findViewById(R.id.textView);
        final TextView aaa = (TextView) findViewById(R.id.textView2);
        DatabaseReference databaseReference1 = FirebaseDatabase.getInstance().getReference();
        DatabaseReference referencee = databaseReference1.child("gerasjson").child("foods");
        referencee.orderByValue().equalTo("2")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for(DataSnapshot data : dataSnapshot.getChildren()){
                            Model model = data.getValue(Model.class);
                            String name = model.getName();
                            tvData.setText(name);
                        }

                        // do whatever with product
                        // }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        tvData.setText("notworking");

                    }
                });
    }
}

